# A comment, this is so you can read your program later.
# Anything after the # is ignored by ruby.

puts "I Could have code like this." # and the comment after is ignored.

# You can also use a commnet to "disable" of comment out a piece of cod:
# puts "This won't run."

puts "This will run."
