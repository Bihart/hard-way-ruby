i = 0
numbers = []

while i < 6
  puts "At the top i is #{i}"
  numbers << i

  i += 1
  puts "Numbers now: ", numbers
  puts "At the bottom i is #{i}"
end

puts "The numbers: "

# remember you can write this 2 other ways
numbers.each {|num| puts num}

# other way 1 [ Ruby style ]
# numbers.each do |num|
#   puts num
# end

# other way 2 [ traditional style ]
# for num in numbers
#   puts num
# end
