# Loops and Arrays
the_count = [1,2,3,4,5]
fruits = ['apples','oranges','pears','apricots']
change = [1, 'pannies', 2, 'dimes', 'quarters']

# this first kind of for-loop goes through a list
# in a more traditional style found in other languages
for number in the_count
  puts "This is count #{number}"
end

# same as above, but in a more Ruby style
# this and the next one are the preferred
# way Ruby for-loops are writeen
fruits.each do |fruit|
  puts "A fruit of type: #{fruit}"
end


# we can also build lists, first start with an empty one
elements = []

# then use the range operator to do 0 to 5 counts
(0..5).each do |i|
  puts "adding #{i} to the list."
  # pushes teh i variable on the *end* of the list
  elements << i
end

# now we can print them out too
elements.each {|i| puts "Element was: #{i}"}
