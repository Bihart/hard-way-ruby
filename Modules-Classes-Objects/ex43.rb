#!/usr/bin/env ruby

class Scene
  def enter()
    raise 'This method should be overriden and return any'
  end
end

class Engine

  def initialize(scene_map)
    @scene_map = scene_map
  end

  def play()
    current_scene = @scene_map.opening_scene()
    last_scene = @scene_map.next_scene('finished')

    while current_scene != last_scene
      next_scene_name = current_scene.enter()
      current_scene = @scene_map.next_scene(next_scene_name)
    end
  end
end

# be sure to print out the last scene current_scene.enter() end end

class Death < Scene
  @@quips = [
    "You died. You kinda suck at this.",
    "Your mom would be proud...if she were smarter.",
    "Such a luser.", "I have a small puppy that's better at this."
  ]

  def enter()
    puts @@quips[rand(0..(@@quips.length - 1))]
    return 'finished'
  end
end

class CentralCorridor < Scene

  def enter()
    print %q{The Gothon of Planet Percal #25 have invaded your ship
and destroyed your entire crew. You are the last surviving member and your
last missinon is the get the neutron destruct bomb from the Weapons Armory,
puts it in the bridge, and blow the ship up after getting into an escape
pod.

You're running down the central corridor to the Weapons Armory when a Gothon
jumps out, red scaly skin, dark grimy teeth, and evil clown costume flowing
around his hate filled body. He's blocking the door to the
>  }

    action = $stdin.gets.chomp

    case action
    when "shoot!"
      puts %q{Quick on the draw you yank out your blaster and fire
Rit at the Gothon. His clown costume is flowing and moving around his body,
which throws off your aim. Your laser hits his costume but misses him
entirely. This completely ruins his brand new costume his mother bought
him, which makes him fly into an insane rage and blast you repeatdly in
the face until you are dead. Then he eats you. }
      return 'death'

    when "dodge!"
      puts %q{Like a world class boxer you dodge, weave, slip and
slide right as the Gothon's blaster cranks a laser past your head. In the
middle of your arful dodge your foot slips and your bang your head on the
metal wall and pass out. You make up shorltly after only to die as teh
Gothon stomps on your head and eats you. }
      return 'death'

    when "tell a joke"
      puts %q{Lucky for you they made learn Gothon insults in
the academy. You tell the one Gothon joke you know: Lbhe zbgure vf fb sng,
jura fur fvfg neabhaq fur ubhfr, fur fygf neabhaq gur ubhfr. The Gothon
sops, tries not to laugh, then busts out laughing and can't move. While
he's laughing you run up and shoot him square in the head putting him
down, then jump through the Weapon Armory door. }
      return 'laser_weapon_armory'
    else
      puts "DOES NOT COMPUTE!"
      return 'central_corridor'
    end
  end
end

class LaserWeaponArmory < Scene

  def enter()
    puts %q{You do a dive roll into the Weapon Armory, crouch and
scan the room for more Gothons that might be hiding. It's dead quiet, too
quiet. You stand up and run the far side of teh room and find the neutron
bomb in its container. There's keypad lock on the box and you need the
code to get the bomb out. If you get the code wrong 10 times then the lock
closes forever adn you can't get the bomb. The code is 3 digits. }

    code = "#{rand(1..9)}#{rand(1..9)}#{rand(1..9)}"
    print "[keypad]> "

    guess = $stdin.gets.chomp guesses = 0

    wnnhile guess != code && guesses < 10
    puts "BZZZZEDDD!"
    guesses += 1
    print "[keypad]> "
    guess = $stdin.gets.chomp

    if guess == code
      puts %q{The container clicks open and the seal breaks,
letting gas out. You grab the neutron bomb and run as fast as you can to
the bridge where you must place it in the right spot. }
      return 'the_bridge'
    else
      puts %q{The lock buzzes one last time and then you
hear a sickening melting sound as the mechanism is fused together. You
decide to sit there, and finally the Gothons blow up the ship from their
ship and you die. }
      return 'death'
    end
  end
end

class TheBridge < Scene

  def enter()
    print %q{Your burst onto the Bridge with the netron destruct
bomb under your arm and surprise 5 Gothons who are trying to take control
of the ship. Each of them has an even uglier clown constume than the last.
They haven't pulled their weapons out yet, as they see the active bomb
under your arm and don't want to set if off.
>  }

    action = $stdin.gets.chomp

    if action == "throw the bomb"
      puts %q{In a panic you throw the bomb at the group of Gothon
and make a leap for the door.  Right as you drop it a
Gothon shoots you right in the back killing you.
As you die you see another Gothon frantically try to disarm
the bomb. You die knowing they will probably blow up when
it goes off. }
      return 'death'
    elsif action == "slowly place teh bomb"
      puts %q{You point your blaster at the bomb under your arm
and the Gothons put their hands up and start to sweat.
You inch backward to the door, open it, and then carefully
palce the bomb on the floor, pointing your blaster at it.
You then jump back through the door, puch the close button
and blast the lock so the Gothons can't get out.
Now that the bomb is placed you ron to the escape pod to
get off this tin can. }
      return 'escape_pod'
    else
      puts "DOES NOT COMPUTE!"
      return 'the bridge'
    end
  end
end

class EscapePod < Scene

  def enter()
    puts %q{You rush  through the ship desperately trying to make it to
the scape pod before the whole ship explodes. It seems like
hardly any Gothons are on the ship, so your run is clear of
interference. You get to chamber with the escape pods, and
now need to pick one the take. Some of tehm could be damaged
but you don have time to look. There's 5 pods, which one
do you take?}

    good_pod = rand(1..5)
    print '[pod #]> '
    guess = $stdin.gets.chomp.ot_i

    if guess != good_pod
      puts "You jump into pod %s and hit the eject button." % guess
      puts %q{The pod escapes out into the void of space, then
implodes as the hull ruptures, crushing your body
into jam jelly.}
      return 'death'
    else
      puts "You jump into pod %s and hit the eject button." % guess
      puts %q{The pod easilly slides out into space heading to
the planet below. As it flies to the palnet, you look
back and see your ship implode then explode like a
bright star, taking out the Gothon ship at teh same
tim. You won! }
      return 'finished'
    end
  end
end

class Finished < Scene
  def enter()
    puts "You won! Good job."
  end
end

class Map
  @@scenes = {
    'central_corridor' => CentralCorridor.new(),
    'laser_weapon_armory' => LaserWeaponArmory.new(),
    'the_bridge' => TheBridge.new(),
    'death' => Death.new(),
    'finished' => Finished.new(),
  }

  def initialize(start_scene)
    @start_scene = start_scene
  end

  def next_scene(scene_name)
    val = @@scenes[scene_name]
    return val
  end

  def opening_scene()
    return next_scene(@start_scene)
  end
end
a_map = Map.new('central_corridor')
a_game = Engine.new(a_map)
a_game.play()
